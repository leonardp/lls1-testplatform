# lls1 platform for testing and programming

# test procedure
* step 3 and 4 are optional (no esp-01m)

## 0 initialize

* set gpios to initial state
* activate 'waiting' led
* activate 'ok' led
* user inserts device
* user presses button

## 1. Voltages

* activate 'voltages' led
* verify_voltages(nobatt_nopower)
* activate loadswitch
* verify_voltages(nobatt_power)

## 2. Periphals

* activate 'periphals' led
* verify_periphals()

## 3. Firmware

* activate 'firmware' led
* gpio -> reset_boot
* erase_flash()
* gpio -> reset_boot
* upload_firmware()
* gpio -> reset

## 4. Testcode

* activate 'testcode' led
* gpio -> reset
* verify_testcode()

## 5. B2RY!

* turn off loadswitch
* activate 'b2ry!' led
* activate 'waiting' led
* user inserts battery
* user presses button
* verify_voltages(batt_power)
* turn on loadswitch
* verify_voltages(batt_nopower)
* STATE -> 0
* user dismounts device and battery
