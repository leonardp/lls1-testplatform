# ZMQ GPIO Thread
import zmq
from threading import Thread
from gpio_thread import gpio_thread

# I2C and testplatform device drivers
from tpic2810 import TPIC2810
from ads1015 import ADS1015
from smbus2 import SMBus

# Testplatform util functions
from test_utils import *

# zZz
from time import sleep
zzz = 0.15

# Setup ZMQ Context and Socket
# for GPIO Thread communication
context = zmq.Context()
gpio = context.socket(zmq.PAIR)
gpio.bind("inproc://@gpiothread")
poller = zmq.Poller()
poller.register(gpio, zmq.POLLIN)

# Start GPIO Thread
gpio_t = Thread(target=gpio_thread, args=(context, ))
gpio_t.start()

#
# VOLTAGE TARGETS
#
# ADC Channel 0    1    2    3
# Pin         3.3  1.8  BAT  5vSolOut
#
voltages_nobatt_nopower = [0.0, 0.0, 0.0, 0.0]
voltages_nobatt_power   = [3.3, 1.8, 0.0, 5.0]
voltages_batt_nopower   = [3.3, 1.8, 4.2, 0.0]
voltages_batt_power     = [3.3, 1.8, 4.2, 5.0]
voltages_fw_test        = [3.0, 2.0, 0.0, 5.0]

#
# Flags 'n States
#
b2ry_inserted = False
wait_rtc_int = False
rtc_int_asserted = False
fail = False

STATES_ALL = ["initialize", "voltages", "periphals", "firmware", "testcode", "b2ry"]
STATES_NOESP = ["initialize", "voltages", "periphals", "b2ry"]
STATES = STATES_NOESP
STATENO = 0

try:
    # Initial Setup
    i2c = SMBus(1)
    leds = TPIC2810(i2c)
    adc = ADS1015(i2c)
    leds.greet()

    while True:
        # Set the state to current statenumber
        STATE = STATES[STATENO]

        # Poll for GPIO thread messages
        sock = dict(poller.poll(100))
        if gpio in sock and sock[gpio] == zmq.POLLIN:
            msg = gpio.recv_string()
            if msg == "button_pressed":
                if STATE == "initialize":
                    STATENO += 1
                    continue
                elif STATE == "b2ry" and b2ry_inserted == False:
                    b2ry_inserted = True
                else:
                    fail = False
                    STATENO = 0
                    continue
            elif msg == "rtc_int_asserted":
                rtc_int_asserted = True
            else:
                print("unknown msg from gpio thread.")

        if fail:
            leds.led_on(["fail", STATE])
            continue

        if STATE == "initialize":
            gpio.send_string('init')
            leds.led_on(["ok", "waiting"])

        if STATE == "voltages":
            leds.led_on(STATE)
            if not verify_voltages(adc, voltages_nobatt_nopower):
                fail = True
                continue
            gpio.send_string('power_on')
            sleep(zzz)
            if not verify_voltages(adc, voltages_nobatt_power):
                fail = True
                continue
            STATENO += 1

        if STATE == "periphals":
            leds.led_on(STATE)
            if not verify_periphals(i2c):
                fail = True
                continue
            STATENO += 1

        if STATE == "firmware":
            leds.led_on(STATE)
            gpio.send_string('reset_boot')
            sleep(zzz)
            if not erase_flash():
                fail = True
                continue
            gpio.send_string('reset_boot')
            sleep(zzz)
            if not upload_firmware("lls1_fw_test.bin"):
                fail = True
                continue
            STATENO += 1

        if STATE == "testcode":
            leds.led_on(STATE)
            if not wait_rtc_int:
                gpio.send_string('reset')
                if not verify_testcode():
                    fail = True
                    continue
                if not verify_voltages(adc, voltages_fw_test):
                    fail = True
                    continue
                gpio.send_string('check_rtc')
                wait_rtc_int = True
            else:
                if not rtc_int_asserted:
                    continue
                else:
                    gpio.send_string('reset_boot')
                    sleep(zzz)
                    if not upload_firmware("lls1_fw_prod.bin"):
                        fail = True
                        continue
                    wait_rtc_int = False
                    rtc_int_asserted = False
                    STATENO += 1

        if STATE == "b2ry":
            if not b2ry_inserted:
                leds.led_on([STATE, "waiting"])
                pass
            else:
                leds.led_on(STATE)
                gpio.send_string('power_off')
                sleep(zzz)
                if not verify_voltages(adc, voltages_batt_nopower):
                    fail = True
                    continue
                gpio.send_string('power_on')
                sleep(zzz)
                if not verify_voltages(adc, voltages_batt_power):
                    fail = True
                    continue
                b2ry_inserted = False
                STATENO = 0

except Exception:
    raise

finally:
    i2c.close()
    gpio.send_string('pls_exit')
    gpio.close()
    gpio_t.join()
    context.term()
