from time import sleep

class TPIC2810(object):
    DEV_ADDR=0x60
    REG_ADDR=0x44

    led_map = {
        'voltages': 0b00001000,
        'periphals':0b00000100,
        'firmware': 0b00000010,
        'testcode': 0b00000001,
        'b2ry':     0b10000000,
        'ok':       0b01000000,
        'waiting':  0b00100000,
        'fail':     0b00010000,
    }
    def __init__(self, i2c):
        self.i2c = i2c
        self.led_off()

    def greet(self):
        ledlist = ['voltages', 'periphals', 'firmware', 'testcode', 'b2ry', 'ok', 'waiting', 'fail']
        for x in range(0,4):
            for led in ledlist:
                self.i2c.write_byte_data(self.DEV_ADDR, self.REG_ADDR, self.led_map[led])
                sleep(0.05)
            ledlist.reverse()

    def led_off(self):
        self.i2c.write_byte_data(self.DEV_ADDR, self.REG_ADDR, 0)

    def led_on(self, led):
        val = 0
        if isinstance(led, list):
            for l in led:
                val |= self.led_map[l]
        else:
            val = self.led_map[led]
        self.i2c.write_byte_data(self.DEV_ADDR, self.REG_ADDR, val)
