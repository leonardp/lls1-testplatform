def verify_voltages(adc, target_voltages):
    volts = adc.read_channels()
    print("### Voltage Measurement ###")
    print("TARGET:   ", target_voltages)
    print("MEASURED: ", volts)
    print("###########################")
    return volts == target_voltages
    #return True


def verify_periphals(i2c):
    from subprocess import run
    cmd = "i2cdetect -y 1 | tail -8 | sed -e 's/--//g' -e 's/[0-9].://g'"
    i2c_raw = run(cmd, shell=True, capture_output=True).stdout.decode().split()
    i2c_scan = [int(x, 16) for x in i2c_raw]

    print("### I2C Scan ###")
    print(i2c_scan)
    print("################")

    devs = [0x68, 0x46] # rtc, adp
    for d in devs:
        if not d in i2c_scan:
            return False
    return True


def erase_flash():
    from subprocess import call
    cmd = "esptool.py -p /dev/ttyS1 erase_flash"
    ret = call(cmd, shell=True)
    if ret:
        return False
    return True


def upload_firmware(name):
    from subprocess import call
    cmd = f"esptool.py -p /dev/ttyS1 -b 460800 write_flash -fs 1MB -fm dout 0x0 {name}"
    ret = call(cmd, shell=True)
    if ret:
        return False
    return True


def upload_testcode():
    """
    this function is not used since ampy is unreliable and therefore not suited for automation
    -> the testscripts need to be included in the mpy firmware
    """
    from subprocess import call
    mp_files = ["main.py", "test.py"]
    for f in mp_files:
        cmd = f"ampy -d 0.2 -b 115200 -p /dev/ttyS1 put mpy_scripts/{f}"
        ret = call(cmd, shell=True)
        if ret:
            return False
    return True


def verify_testcode():
    import serial, time
    with serial.Serial('/dev/ttyS1', 115200, timeout=1) as ser:
        tic = time.time()
        toc = time.time()
        while (toc-tic < 5):
            l = ser.readline()
            if l == "TEST_OK":
                return True
            if l == "TEST_FAIL":
                return False
            toc = time.time()
    return False
