def gpio_thread(zmq_context):

    import sys, zmq
    from periphery import GPIO

    from time import sleep
    z = 0.05
    zzz = 0.25

    # zmq_context = zmq.Context()
    socket = zmq_context.socket(zmq.PAIR)
    socket.connect("inproc://@gpiothread")

    poller = zmq.Poller()
    poller.register(socket, zmq.POLLIN)

    GPIOCHIP = "/dev/gpiochip0"

    gpio_map = {
        "IO0": (17, "out"),# ESP IO0 (boot mode)
        "EN":  (27, "out"),# ESP EN
        "IO16":( 5, "in"), # ESP IO16 (deep sleep wakeup)
        "RST": ( 6, "out"),# ESP RST
        "RTC": (19, "in"), # RTC interrupt output
        "SW":  (21, "in"), # Pushbutton
        "LS":  (26, "out"),# Loadswitch
    }

    gpio_initial = [
            ("IO0",  True),
            ("EN",   True),
            ("RST",  True),
            ("LS",   False),
    ]

    # Initialize GPIO objects
    gpios = {}
    for k, v in gpio_map.items():
        gpios[k] = GPIO(GPIOCHIP, *v)

    # Flags
    poll_rtc = True

    while True:
        msg = ""

        sock = dict(poller.poll(100))
        if socket in sock and sock[socket] == zmq.POLLIN:
            msg = socket.recv_string()
            if msg == 'pls_exit':
                socket.close()
                sys.exit(0)

        # read SW state, sleep to 'debounce'
        if not gpios["SW"].read():
            socket.send_string("button_pressed")
            sleep(zzz)

        if msg == "init":
            for gpio, state in gpio_initial:
                gpios[gpio].write(state)

        if msg == "power_on":
            gpios["LS"].write(True)

        if msg == "power_off":
            gpios["LS"].write(False)

        if msg == "reset":
            gpios["EN"].write(False)
            sleep(z)
            gpios["EN"].write(True)

        if msg == "reset_boot":
            gpios["IO0"].write(False)
            gpios["EN"].write(False)
            sleep(z)
            gpios["EN"].write(True)
            sleep(zzz)
            gpios["IO0"].write(True)

        if msg == "check_rtc":
            poll_rtc = True

        if poll_rtc and not gpios["RTC"].read():
            socket.send_string("rtc_int_asserted")
            poll_rtc = False

