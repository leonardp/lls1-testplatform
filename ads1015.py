from smbus2 import SMBus
from time import sleep


class ADS1015:
    """BIG ENDIAN device
    """

    DEV_ADDR = 0x48
    CONV_REG = 0x00
    CONF_REG = 0x01
    # measures channel: 2/3 gain, continuous mode ->
    channels = (
        0b1000001101000000,
        0b1000001101010000,
        0b1000001101100000,
        0b1000001101110000,
    )
    v_conv = 0.000187506

    def __init__(self, i2c):
        self.i2c = i2c

    def read_channels(self):
        data = []
        for ch in range(0, 4):
            data.append(self.get_channel(ch))
        return data

    def get_channel(self, ch):
        self.i2c.write_word_data(self.DEV_ADDR, self.CONF_REG, self.channels[ch])
        sleep(0.05)
        vals = []
        for i in range(0, 3):
            raw = self.i2c.read_i2c_block_data(self.DEV_ADDR, self.CONV_REG, 2)
            vals.append((((raw[0] & 0xFF) << 8) | (raw[1] & 0xFF)))
            sleep(0.01)
        vals.sort()
        return round(self.v_conv * vals[1], 1)


i2c = SMBus(1)
